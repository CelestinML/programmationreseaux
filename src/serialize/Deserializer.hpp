#pragma once

#include <vector>

namespace uqac::serialize {
	class Deserializer {
	private:
		int m_position;

		std::vector<char> m_container;

	public:
		Deserializer(std::vector<char> container);
		~Deserializer();

		template <typename T, std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>>* = nullptr>
		size_t Deserialize(T& destVar) {
			size_t size = sizeof(T);

			if (m_position + size <= m_container.size()) {
				std::memcpy((char*)&destVar, m_container.data() + m_position, size);

				m_position += size;
				return size;
			}
			else {
				return -1;
			}
		}

		bool isEmpty();

		inline void clear() { m_container.clear(); }
	};
}