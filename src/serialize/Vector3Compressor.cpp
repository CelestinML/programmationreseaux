#include "Vector3Compressor.hpp"
#include <iostream>

using namespace uqac::serialize;
using namespace uqac::game;

Vector3Compressor::Vector3Compressor(float x_min, float x_max, float y_min, float y_max, float z_min, float z_max) :
	m_x_floatCompressor(x_min, x_max),
	m_y_floatCompressor(y_min, y_max),
	m_z_floatCompressor(z_min, z_max)
{ }

void Vector3Compressor::compress(Serializer& serializer, Vector3 value)
{
	m_x_floatCompressor.compress(serializer, value.getX());
	m_y_floatCompressor.compress(serializer, value.getY());
	m_z_floatCompressor.compress(serializer, value.getZ());
}

Vector3 Vector3Compressor::decompress(Deserializer& deserializer)
{
	float xx = m_x_floatCompressor.decompress(deserializer);
	float yy = m_y_floatCompressor.decompress(deserializer);
	float zz = m_z_floatCompressor.decompress(deserializer);

	return Vector3(xx, yy, zz);
}