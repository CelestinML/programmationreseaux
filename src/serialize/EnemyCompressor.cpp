#include "EnemyCompressor.h"
#include <string.h>
#include <iostream>

using namespace uqac::serialize;
using namespace uqac::game;
using namespace std;

void EnemyCompressor::compress(Serializer& serializer, Enemy enemy) {
	//cout << "Compressing position : " << enemy.getPosition().ToString() << endl;
	m_positionCompressor.compress(serializer, enemy.getPosition());
	//cout << "Compressing type : " << enemy.getEnemyType() << endl;
	m_typeCompressor.compress(serializer, enemy.getEnemyType());
	//cout << "Compressing rotation : " << enemy.getRotation().toString() << endl;
	m_rotationCompressor.compress(serializer, enemy.getRotation());
	//cout << "Compressing vie : " << enemy.getVie() << endl;
	m_vieCompressor.compress(serializer, enemy.getVie());
}

Enemy EnemyCompressor::decompress(Deserializer& deserializer) {
	Vector3 position = m_positionCompressor.decompress(deserializer);
	int typeI = m_typeCompressor.decompress(deserializer);
	Quaternion rotation = m_rotationCompressor.decompress(deserializer);
	int vie = m_vieCompressor.decompress(deserializer);

	bool isBoss = true;
	if(typeI == 0) isBoss = false;

	return Enemy(position, isBoss, rotation, vie);
}