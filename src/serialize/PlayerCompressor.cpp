#include "PlayerCompressor.hpp"
#include <string.h>
#include <iostream>

using namespace uqac::serialize;
using namespace uqac::game;
using namespace std;

void PlayerCompressor::compress(Serializer& serializer, Player player) {
	//cout << "Compressing position : " << player.getPosition().ToString() << endl;
	m_positionCompressor.compress(serializer, player.getPosition());
	//cout << "Compressing rotation : " << player.getRotation().toString() << endl;
	m_rotationCompressor.compress(serializer, player.getRotation());
	//cout << "Compressing taille : " << player.getTaille().ToString() << endl;
	m_tailleCompressor.compress(serializer, player.getTaille());
	//cout << "Compressing vie : " << player.getVie() << endl;
	m_vieCompressor.compress(serializer, player.getVie());
	//cout << "Compressing armure : " << player.getArmure() << endl;
	m_armureCompressor.compress(serializer, player.getArmure());
	//cout << "Compressing Argent : " << player.getArgent() << endl;
	m_argentCompressor.compress(serializer, player.getArgent());
	//cout << "Compressing nom : " << player.getNom() << endl;
	m_nomCompressor.compress(serializer, player.getNom(), strlen(player.getNom()));
}
Player PlayerCompressor::decompress(Deserializer& deserializer) {
	Vector3 position = m_positionCompressor.decompress(deserializer);
	Quaternion rotation = m_rotationCompressor.decompress(deserializer);
	Vector3 taille = m_tailleCompressor.decompress(deserializer);
	int vie = m_vieCompressor.decompress(deserializer);
	int armure = m_armureCompressor.decompress(deserializer);
	float argent = m_argentCompressor.decompress(deserializer);
	char* nom = m_nomCompressor.decompress(deserializer);

	return Player(position, rotation, taille, vie, armure, argent, nom);
}