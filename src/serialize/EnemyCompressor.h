#pragma once

#include "QuaternionCompressor.hpp"
#include "StringCompressor.hpp"
#include "Vector3Compressor.hpp"
#include "../game/Enemy.hpp"

namespace uqac::serialize {
	using namespace uqac::game;
	class EnemyCompressor {

		enum EnemyType {
			Boss,
			Sbire
		};
	private:
		Vector3Compressor m_positionCompressor = Vector3Compressor(-500, 500, -500, 500, 0, 100);
		IntCompressor m_typeCompressor = IntCompressor(0, 1);
		QuaternionCompressor m_rotationCompressor = QuaternionCompressor();
		IntCompressor m_vieCompressor = IntCompressor(0, 1000);
	public:
		void compress(Serializer& serializer, Enemy enemy);
		Enemy decompress(Deserializer& deserializer);
	};

}