#include "FloatCompressor.hpp"
#include <iostream>

using namespace uqac::serialize;

FloatCompressor::FloatCompressor(float min, float max) : intCompressor((int)min * 1000, (int)max * 1000)
{ }

void FloatCompressor::compress(Serializer& serializer, float value) 
{
	int intValue = value * 1000;
	intCompressor.compress(serializer, intValue);
}

float FloatCompressor::decompress(Deserializer &deserializer) 
{
	int res = intCompressor.decompress(deserializer);
	float floatRes = (float)res / 1000.0f;
	return floatRes;
}