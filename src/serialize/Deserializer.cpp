#include "Deserializer.hpp"

using namespace uqac::serialize;

Deserializer::Deserializer(std::vector<char> container) :
	m_container(container), m_position(0)
{}

Deserializer::~Deserializer() {
	m_container.clear();
}

bool Deserializer::isEmpty() {
	if (m_position >= m_container.size()-1) {
		return true;
	}
	else {
		return false;
	}
}