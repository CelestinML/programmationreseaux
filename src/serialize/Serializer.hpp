#pragma once

#include <vector>
#include <type_traits>
#include <iostream>

namespace uqac::serialize {

	class Serializer {
	private:

		int m_position;
		std::vector<char> m_container;

	public:

		Serializer(int size);
		Serializer();
		~Serializer();

		inline std::vector<char> getContainer() { return m_container; }

		template <typename T, std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>>* = nullptr>
		void Serialize(T toSerialize) {
			int size = sizeof(toSerialize);
			if (m_position + size > m_container.size()) {
				//We have to increase our container's size
				m_container.resize(m_position + size);
			}
			std::memcpy(m_container.data() + m_position, reinterpret_cast<char*>(&toSerialize), size);
			m_position += size;
		}

		inline void writeIntAtIndex(int index, int value) { m_container[index] = value; }

		void clear();

		inline int getPosition() const { return m_position; }
	};
}