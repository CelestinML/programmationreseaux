#include "Serializer.hpp"

using namespace uqac::serialize;

Serializer::Serializer(int size) {
	m_container = std::vector<char>(size);
	m_position = 0;
}

Serializer::Serializer() {
	m_position = 0;
}

Serializer::~Serializer() {
	clear();
}

void Serializer::clear() {
	m_position = 0;
	m_container.clear();
	m_container.resize(0);
}