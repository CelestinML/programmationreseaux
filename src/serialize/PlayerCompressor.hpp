#pragma once

#include "QuaternionCompressor.hpp"
#include "StringCompressor.hpp"
#include "Vector3Compressor.hpp"
#include "../game/Player.hpp"

namespace uqac::serialize {
	using namespace uqac::game;
	class PlayerCompressor {
	private:
		Vector3Compressor m_positionCompressor = Vector3Compressor(-500,500,-500,500,0,100);
		QuaternionCompressor m_rotationCompressor = QuaternionCompressor();
		Vector3Compressor m_tailleCompressor = Vector3Compressor(0, 10, 0 , 10, 0, 10);
		IntCompressor m_vieCompressor = IntCompressor(0,300);
		IntCompressor m_armureCompressor = IntCompressor(0,50);
		FloatCompressor m_argentCompressor = FloatCompressor(-99999.99, 99999.99);
		StringCompressor m_nomCompressor = StringCompressor();
	public:
		void compress(Serializer& serializer, Player player );
		Player decompress(Deserializer& deserializer);
	};

}