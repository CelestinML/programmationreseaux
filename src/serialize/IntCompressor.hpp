#pragma once

#include "Serializer.hpp"
#include "Deserializer.hpp"

namespace uqac::serialize {
	class IntCompressor {
	private:
		int m_min; 
		int m_max;
		/*
		Ranges :
		- 255 -> 8 bits
		- 65535 -> 16 bits
		- 4294967295 -> 32 bits
		- 18446744073709551615 -> 64 bits
		*/
		int m_maxRange;

		int returnInt(size_t r, int i);

	public:
		IntCompressor(int min, int max);

		void compress(Serializer &serializer, int value);

		int decompress(Deserializer &deserializer);
	};
}