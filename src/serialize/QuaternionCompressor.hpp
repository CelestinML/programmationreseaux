#pragma once

#include "Serializer.hpp"
#include "Deserializer.hpp"
#include "FloatCompressor.hpp"
#include "../game/Quaternion.hpp"

namespace uqac::serialize {
	using namespace uqac::game;
	class QuaternionCompressor {
	private:
		// compressor of the smallest value's index (<=> a, b, c or d)
		IntCompressor m_indexCompressor = IntCompressor(0, 3); 
		FloatCompressor m_floatCompressor = FloatCompressor(-1,1);
	public:
		void compress(Serializer& serializer, Quaternion quaternion);
		Quaternion decompress(Deserializer& deserializer);
	};

}

