#pragma once

#include "Serializer.hpp"
#include "Deserializer.hpp"

namespace uqac::serialize {
	class StringCompressor {
	private:
		uint16_t m_length;

	public:

		void compress(Serializer& serializer, char* value, uint16_t length);

		char* decompress(Deserializer& deserializer);
	};
}