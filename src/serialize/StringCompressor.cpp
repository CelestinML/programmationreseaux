#include "StringCompressor.hpp"
#include <iostream>

using namespace uqac::serialize;

void StringCompressor::compress(Serializer& serializer, char* value, uint16_t length) 
{
	m_length = length;

	serializer.Serialize(length);
	for (int i = 0; i < length; i++)
	{
		serializer.Serialize(value[i]);
	}
}

char* StringCompressor::decompress(Deserializer& deserializer) 
{
	uint16_t readValue;
	deserializer.Deserialize(readValue);

	char* res = new char[readValue];
	for (int i = 0; i < readValue; i++)
	{
		char add;

		deserializer.Deserialize(add);
		res[i] = add;
	}
	res[readValue] = '\0';

	return res;
}