#include "QuaternionCompressor.hpp"
#include <math.h>  
#include <algorithm>
#include <iostream>

using namespace uqac::serialize;
using namespace uqac::game;

void QuaternionCompressor::compress(Serializer& serializer, Quaternion quaternion){
	std::vector<float> listvalues = { quaternion.getA(), quaternion.getB(), quaternion.getC(), quaternion.getD() };
	float smallest = std::min({ quaternion.getA(), quaternion.getB(), quaternion.getC(), quaternion.getD() });
	int smallest_index = 0;
	for (int i = 0; i < listvalues.size(); i++) {
		if (listvalues[i] == smallest) {
			smallest_index = i;
			break;
		}
	}
	// smallest is the float we will not compress and serialize
	m_indexCompressor.compress(serializer, smallest_index);
	for (int i = 0; i < listvalues.size(); i++) {
		if (i != smallest_index) {
			m_floatCompressor.compress(serializer, listvalues[i]);
		}
	}
}


Quaternion QuaternionCompressor::decompress(Deserializer& deserializer) {
	int smallest_index = m_indexCompressor.decompress(deserializer);
	float a = 2, b = 2, c = 2, d = 2;
	// Finding the smallest value's index
	if (smallest_index != 0) {	
		a = m_floatCompressor.decompress(deserializer);
	}
	if (smallest_index != 1) {
		b = m_floatCompressor.decompress(deserializer);
	}
	if (smallest_index != 2) {
		c = m_floatCompressor.decompress(deserializer);
	}
	if (smallest_index != 3) {
		d = m_floatCompressor.decompress(deserializer);
	}

	//Calculating the missing value
	if (a == 2) {
		a = sqrtf(1 - (b * b + c * c + d * d));
	}
	else if (b == 2) {
		b = sqrtf(1 - (a * a + c * c + d * d));
	}
	else if (c == 2) {
		c = sqrtf(1 - (a * a + b * b + d * d));
	}
	else if (d == 2) {
		d = sqrtf(1 - (a * a + c * c + b * b));
	}

	return Quaternion(a, b, c, d);
}