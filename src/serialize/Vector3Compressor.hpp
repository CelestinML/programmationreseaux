#pragma once

#include "Serializer.hpp"
#include "Deserializer.hpp"
#include "FloatCompressor.hpp"
#include "../game/Vector3.hpp"

namespace uqac::serialize {
	using namespace uqac::game;
	class Vector3Compressor {
	private:
		/*
		Ranges :
		- 255 -> 8 bits
		- 65535 -> 16 bits
		- 4294967295 -> 32 bits
		- 18446744073709551615 -> 64 bits
		*/
		FloatCompressor m_x_floatCompressor;
		FloatCompressor m_y_floatCompressor;
		FloatCompressor m_z_floatCompressor;

	public:
		Vector3Compressor(float x_min, float x_max, float y_min, float y_max, float z_min, float z_max);

		void compress(Serializer& serializer, Vector3 value);

		Vector3 decompress(Deserializer& deserializer);
	};
}