#include "IntCompressor.hpp"
#include <iostream>

using namespace uqac::serialize;

IntCompressor::IntCompressor(int min, int max) :
	m_max(max), m_min(min)
{
	int maxValue = max;
	if (min < 0) {
		maxValue = m_max + std::abs(min);
	}

	if (maxValue <= UINT8_MAX) {
		m_maxRange = 1;
	}
	else if (maxValue <= UINT16_MAX) {
		m_maxRange = 2;
	}
	else if (maxValue <= UINT32_MAX) {
		m_maxRange = 4;
	}
	else {
		m_maxRange = 8;
	}
}

void IntCompressor::compress(Serializer &serializer, int value) {
	int toSerialize = value;
	if (m_min < 0) {
		toSerialize = value + std::abs(m_min);
	}
	if (m_maxRange == 1) {
		serializer.Serialize((uint8_t)toSerialize);
	}
	else if (m_maxRange == 2) {
		serializer.Serialize((uint16_t)toSerialize);
	}
	else if (m_maxRange == 4) {
		serializer.Serialize((uint32_t)toSerialize);
	}
	else {
		serializer.Serialize((uint64_t)toSerialize);
	}
}

int IntCompressor::decompress(Deserializer &deserializer) {
	size_t readSize;

	if (m_maxRange == 1) {
		uint8_t readValue;
		readSize = deserializer.Deserialize(readValue);
		return returnInt(readSize, readValue);
	}
	else if (m_maxRange == 2) {
		uint16_t readValue;
		readSize = deserializer.Deserialize(readValue);
		return returnInt(readSize, readValue);
	}
	else if (m_maxRange == 4) {
		uint32_t readValue;
		readSize = deserializer.Deserialize(readValue);
		return returnInt(readSize, readValue);
	}
	else {
		uint64_t readValue;
		readSize = deserializer.Deserialize(readValue);
		return returnInt(readSize, readValue);
	}
}

int IntCompressor::returnInt(size_t r, int i) {
	if (r != 0) {
		if (m_min < 0) {
			return i - std::abs(m_min);
		}
		else {
			return i;
		}
	}
	else {
		std::cout << "Nothing to read (IntCompressor)" << std::endl;
		exit(EXIT_FAILURE);
	}
}