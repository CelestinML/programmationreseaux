#pragma once

#include "Serializer.hpp"
#include "Deserializer.hpp"
#include "IntCompressor.hpp"

namespace uqac::serialize {
	class FloatCompressor {
	private:
		IntCompressor intCompressor;

	public:
		FloatCompressor(float min, float max);

		void compress(Serializer& serializer, float value);

		float decompress(Deserializer& deserializer);
	};
}