#include "Vector3.hpp"
#include <iostream>

using namespace uqac::game;

Vector3::Vector3(float x, float y, float z)
{
	m_x = x;
	m_y = y;
	m_z = z;
}

float Vector3::getX() const {
	return m_x;
}
float Vector3::getY() const {
	return m_y;
}
float Vector3::getZ() const {
	return m_z;
}

void Vector3::setX(float x) {
	m_x = x;
}
void Vector3::setY(float y) {
	m_y = y;
}
void Vector3::setZ(float z) {
	m_z = z;
}


Vector3 Vector3::operator*(float value)
{
	return Vector3(m_x * value, m_y * value, m_z * value);
}

Vector3 Vector3::operator*=(float value)
{
	m_x *= value;
	m_y *= value;
	m_z *= value;
	return Vector3(m_x, m_y, m_z);
}

Vector3 Vector3::operator/(float value)
{
	return Vector3(m_x / value, m_y / value, m_z / value);
}

Vector3 Vector3::operator/=(float value)
{
	m_x /= value;
	m_y /= value;
	m_z /= value;
	return Vector3(m_x, m_y, m_z);
}

Vector3 Vector3::operator-(Vector3 value)
{
	return Vector3(m_x - value.m_x, m_y - value.m_y, m_z - value.m_z);
}

Vector3 Vector3::operator-=(Vector3 value)
{
	m_x -= value.m_x;
	m_y -= value.m_y;
	m_z -= value.m_z;
	return Vector3(m_x, m_y, m_z);
}

Vector3 Vector3::operator+(Vector3 value)
{
	return Vector3(m_x + value.m_x, m_y + value.m_y, m_z + value.m_z);
}

Vector3 Vector3::operator+=(Vector3 value)
{
	m_x += value.m_x;
	m_y += value.m_y;
	m_z += value.m_z;
	return Vector3(m_x, m_y, m_z);
}

float Vector3::GetNorm()
{
	return sqrtf((m_x * m_x) + (m_y * m_y) + (m_z * m_z));
}

Vector3 Vector3::Normalize()
{
	float norm = GetNorm();
	return Vector3(m_x / norm, m_y / norm, m_z / norm);
}

float Vector3::ScalarProduct(Vector3 prod)
{
	return (m_x * prod.m_x) + (m_y * prod.m_y) + (m_z * prod.m_z);
}

Vector3 Vector3::VectorialProduct(Vector3 prod)
{
	return Vector3(
		(m_y * prod.m_z) - (m_z * prod.m_y),
		(m_z * prod.m_x) - (m_x * prod.m_z),
		(m_x * prod.m_y) - (m_y * prod.m_x)
	);
}

std::string Vector3::ToString()
{
	return "V(" + std::to_string(m_x) + "," + std::to_string(m_y) + "," + std::to_string(m_z) + ")";
}
