#pragma once
#include <math.h>
#include <string>

namespace uqac::game {
	class Vector3
	{
	private:
		float m_x; float m_y; float m_z;

	public:
		Vector3(float x = 0, float y = 0, float z = 0);

		float getX() const;
		float getY() const;
		float getZ() const;

		void setX(float x);
		void setY(float y);
		void setZ(float z);

		Vector3 operator*(float value);
		Vector3 operator*=(float value);
		Vector3 operator/(float value);
		Vector3 operator/=(float value);
		Vector3 operator-(Vector3 value);
		Vector3 operator-=(Vector3 value);
		Vector3 operator+(Vector3 value);
		Vector3 operator+=(Vector3 value);
		float GetNorm();
		Vector3 Normalize();
		float ScalarProduct(Vector3 prod);
		Vector3 VectorialProduct(Vector3 prod);
		std::string ToString();
	};

}