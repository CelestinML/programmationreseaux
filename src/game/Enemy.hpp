#pragma once

#include "Vector3.hpp"
#include "Quaternion.hpp"
#include "../replication/NetworkObject.hpp"

#include <iostream>

namespace uqac::game {
	using namespace uqac::replication;
	class Enemy : public NetworkObject {
	public:
		//Define enemy type
		enum EnemyType {
			Sbire,
			Boss
		};
	private:
		Vector3 m_position = Vector3(0,0,0);
		EnemyType m_enemyType = Sbire;
		Quaternion m_rotation = Quaternion(0,0,0,1);
		int m_vie = 100; // 0 � 1000
	
	public:
		Enemy(Vector3 position, bool isBoss, Quaternion rotation, int vie);
		Enemy() = default;
		~Enemy() = default;

		enum { ClassID = 'ENEM' };

		inline void setPosition(Vector3 position) { m_position = position; }
		inline void setRotation(Quaternion rotation) { m_rotation = rotation; }
		inline void setVie(int vie) { m_vie = vie; }
		inline void setEnemyType(bool isBoss) { if(isBoss) m_enemyType = Boss; else m_enemyType = Sbire; }

		inline Vector3 getPosition() { return m_position; }
		inline Quaternion getRotation() { return m_rotation; }
		inline int getVie() { return m_vie; }
		inline EnemyType getEnemyType() { return m_enemyType; }

		std::string toString();

		//Replication methods
		void write(Serializer& serializer) override;
		void read(Deserializer& deserializer) override;
		void destroy() override;

		int getClassID() override {
			return ClassID;
		};

		void modifyRandomAttribute();
	};
}