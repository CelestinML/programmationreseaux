#include <random>

#include "Player.hpp"
#include "serialize/PlayerCompressor.hpp"

using namespace uqac::game;
using namespace uqac::replication;

Player::Player(Vector3 position, Quaternion rotation, Vector3 taille, int vie, int armure, float argent, char* nom) :
	m_position(position), m_rotation(rotation), m_taille(taille), m_vie(vie), m_argent(argent), m_armure(armure)
{
	m_nom = new char(128);
	m_nom = nom;
}

Player::Player() {
	modifyName();
}

std::string Player::toString() {
	return	"Player :\n - Nom : " + std::string(m_nom) +
		"\n - Position : " + m_position.ToString() +
		"\n - Rotation : " + m_rotation.toString() +
		"\n - Taille : " + m_taille.ToString() +
		"\n - Vie : " + std::to_string(m_vie) +
		"\n - Armure : " + std::to_string(m_armure) +
		"\n - Argent : " + std::to_string(m_argent) +
		"\n";		
}

//Replication methods
void Player::write(Serializer& serializer) {
	PlayerCompressor playerCompressor = PlayerCompressor();

	playerCompressor.compress(serializer, *this);
}

void Player::read(Deserializer& deserializer) {
	PlayerCompressor playerCompressor = PlayerCompressor();
	Player deserializedPlayer = playerCompressor.decompress(deserializer);

	setPosition(deserializedPlayer.getPosition());
	setRotation(deserializedPlayer.getRotation());
	setTaille(deserializedPlayer.getTaille());
	setVie(deserializedPlayer.getVie());
	setArmure(deserializedPlayer.getArmure());
	setArgent(deserializedPlayer.getArgent());
	setNom(deserializedPlayer.getNom());
}

void Player::destroy() {
	delete this;
}

void Player::modifyName() {
	switch (getRandomInt(0, 9)) {
	case 0:
		m_nom = "Henri";
		break;
	case 1:
		m_nom = "Henriette";
		break;
	case 2:
		m_nom = "Josette";
		break;
	case 3:
		m_nom = "Jose";
		break;
	case 4:
		m_nom = "Robert";
		break;
	case 5:
		m_nom = "Jean Yves";
		break;
	case 6:
		m_nom = "Madeleine";
		break;
	case 7:
		m_nom = "Remi";
		break;
	case 8:
		m_nom = "Jean";
		break;
	case 9:
		m_nom = "Louis";
		break;
	default:
		std::cout << "Random returned a wrong number for name" << std::endl;
	}
}

void Player::modifyRandomAttribute() {
	/*
		Nom			0
		Position	1
		Taille		2
		Vie			3
		Armure		4
		Argent		5
	*/

	switch (getRandomInt(0,5)) {
	case 0:
		std::cout << "Updating player's name" << std::endl;
		modifyName();
		break;
	case 1:
		std::cout << "Updating player's position" << std::endl;
		m_position.setX(std::clamp(m_position.getX() + getRandomInt(-10, 10), -500.f, 500.f));
		m_position.setY(std::clamp(m_position.getY() + getRandomInt(-10, 10), -500.f, 500.f));
		m_position.setZ(std::clamp(m_position.getZ() + getRandomInt(-10, 10), 0.f, 100.f));
		break;
	case 2:
		std::cout << "Updating player's size" << std::endl;
		m_taille.setX(std::clamp(m_taille.getX() + getRandomInt(-10, 10), 0.f, 10.f));
		m_taille.setY(std::clamp(m_taille.getY() + getRandomInt(-10, 10), 0.f, 10.f));
		m_taille.setZ(std::clamp(m_taille.getZ() + getRandomInt(-10, 10), 0.f, 10.f));
		break;
	case 3:
		std::cout << "Updating player's life" << std::endl;
		m_vie = std::clamp(m_vie + getRandomInt(-15, 15), 0, 300);
		break;
	case 4:
		std::cout << "Updating player's armor" << std::endl;
		m_armure = std::clamp(m_armure + getRandomInt(-10, 10), 0, 50);
		break;
	case 5:
		std::cout << "Updating player's money" << std::endl;
		m_argent += getRandomInt(-2000, 2000);
		if (m_argent < -99999.99) {
			m_argent = -99999.99;
		}
		else if (m_argent > 99999.99) {
			m_argent = 99999.99;
		}
		break;
	default:
		std::cout << "Random returned a wrong number for index" << std::endl;
	}
}