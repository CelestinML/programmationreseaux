#pragma once
#include <string>

namespace uqac::game {
	class Quaternion {
	private:
		float m_a, m_b, m_c, m_d;
	public:
		Quaternion(float a, float b, float c, float d);
		~Quaternion() = default;

		float getA();
		float getB();
		float getC();
		float getD();

		void setA(float a);
		void setB(float b);
		void setC(float c);
		void setD(float d);

		std::string toString();
	};
}