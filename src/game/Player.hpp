#pragma once

#include "Vector3.hpp"
#include "Quaternion.hpp"
#include "../replication/NetworkObject.hpp"

#include <string>

namespace uqac::game {
	using namespace uqac::replication;
	class Player : public NetworkObject {
	private:
		Vector3 m_position = Vector3(0, 0, 0);
		Quaternion m_rotation = Quaternion(0, 0, 0, 1);
		Vector3 m_taille = Vector3(1, 1, 1);

		int m_vie = 10; // 0 � 300
		int m_armure = 10; // 0 � 50
		float m_argent = 666.66; // -99 999.99 � 99 999.99
		char* m_nom = "Robert"; // 128 bytes

		void modifyName();

	public:
		Player(Vector3 position, Quaternion rotation, Vector3 taille, int vie, int armure, float argent, char* nom);
		Player();
		~Player() = default;

		enum { ClassID = 'PLAY' };

		inline void setPosition(Vector3 position) { m_position = position; }
		inline void setRotation(Quaternion rotation) { m_rotation = rotation; }
		inline void setTaille(Vector3 taille) { m_taille = taille; }
		inline void setVie(int vie) { m_vie = vie; }
		inline void setArmure(int armure) { m_armure = armure; }
		inline void setArgent(float argent) { m_argent = argent; }
		inline void setNom(char* nom) { m_nom = nom; }

		inline Vector3 getPosition() { return m_position; }
		inline Quaternion getRotation() {return m_rotation; }
		inline Vector3 getTaille() { return m_taille; }
		inline int getVie() { return m_vie; }
		inline int getArmure() { return m_armure; }
		inline float getArgent() { return m_argent; }
		inline char* getNom() { return m_nom; }

		std::string toString();

		//Replication methods
		void write(Serializer& serializer) override;
		void read(Deserializer& deserializer) override;
		void destroy() override;

		int getClassID() override {
			return ClassID;
		};

		void modifyRandomAttribute();
	};
}