#include "Enemy.hpp"
#include "serialize/EnemyCompressor.h"

using namespace uqac::game;
using namespace uqac::replication;

Enemy::Enemy(Vector3 position, bool isBoss, Quaternion rotation, int vie) :
	m_position(position), m_rotation(rotation), m_vie(vie)
{
	if (isBoss) m_enemyType = Boss;
	else m_enemyType = Sbire;
}

std::string Enemy::toString() {
	if (m_enemyType == Sbire) {
		return	"Enemy : Sbire\n - Position : " + m_position.ToString() +
				"\n - Rotation : " + m_rotation.toString() +
				"\n - Vie : " + std::to_string(m_vie) +
				"\n";
	}
	else {
		return	"Enemy : Boss\n - Position : " + m_position.ToString() +
			"\n - Rotation : " + m_rotation.toString() +
			"\n - Vie : " + std::to_string(m_vie) +
			"\n";
	}
}

//Replication methods
void Enemy::write(Serializer& serializer) {
	EnemyCompressor enemyCompressor = EnemyCompressor();

	enemyCompressor.compress(serializer, *this);
}

void Enemy::read(Deserializer& deserializer) {
	EnemyCompressor enemyCompressor = EnemyCompressor();
	Enemy deserializedEnemy = enemyCompressor.decompress(deserializer);

	setPosition(deserializedEnemy.getPosition());
	setEnemyType(deserializedEnemy.getEnemyType());
	setRotation(deserializedEnemy.getRotation());
	setVie(deserializedEnemy.getVie());
}

void Enemy::destroy() {
	delete this;
}

void Enemy::modifyRandomAttribute() {
	/*
		Position	0
		Vie			1 -> 0 � 1000
	*/

	switch (getRandomInt(0, 1)) {
	case 0:
		std::cout << "Updating enemy's position" << std::endl;
		m_position.setX(std::clamp(m_position.getX() + getRandomInt(-10, 10), -500.f, 500.f));
		m_position.setY(std::clamp(m_position.getY() + getRandomInt(-10, 10), -500.f, 500.f));
		m_position.setY(std::clamp(m_position.getY() + getRandomInt(-10, 10), 0.f, 100.f));
		break;
	case 1:
		std::cout << "Updating enemy's life" << std::endl;
		m_vie = std::clamp(m_vie + getRandomInt(-15, 15), 0, 1000);
		break;
	default:
		std::cout << "Random returned a wrong number" << std::endl;
	}
}