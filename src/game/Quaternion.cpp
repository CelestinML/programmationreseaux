#include "Quaternion.hpp"

using namespace uqac::game;

Quaternion::Quaternion(float a, float b, float c, float d) : m_a(a), m_b(b), m_c(c), m_d(d) 
{}

float Quaternion::getA() {
	return m_a;
}

float Quaternion::getB() {
	return m_b;
}
float Quaternion::getC() {
	return m_c;
}
float Quaternion::getD() {
	return m_d;
}

void Quaternion::setA(float a) {
	m_a = a;
}
void Quaternion::setB(float b) {
	m_b = b;
}
void Quaternion::setC(float c) {
	m_c = c;
}
void Quaternion::setD(float d) {
	m_d = d;
}

std::string Quaternion::toString() {
	std::string s = std::to_string(m_a) + ", " + std::to_string(m_b) + ", " + std::to_string(m_c) + ", " + std::to_string(m_d);
	return s;
}