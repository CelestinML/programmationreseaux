#pragma once

#include <random>

#include "../serialize/Serializer.hpp"
#include "../serialize/Deserializer.hpp"

namespace uqac::replication {
	using namespace uqac::serialize;
	class NetworkObject {
	protected:
		int getRandomInt(int min, int max) {
			std::random_device rd;     // only used once to initialise (seed) engine
			std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
			std::uniform_int_distribution<int> uni(min, max); // guaranteed unbiased

			return uni(rng);
		}
	public:
		virtual void write(Serializer& serializer) = 0;
		virtual void read(Deserializer& deserializer) = 0;
		virtual void destroy() = 0;

		virtual int getClassID() = 0;

		virtual std::string toString() = 0;

		virtual void modifyRandomAttribute() = 0;
	};
}