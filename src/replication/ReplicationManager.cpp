#include <random>

#include "ReplicationManager.hpp"
#include "game/Player.hpp"
#include "game/Enemy.hpp"

using namespace uqac::replication;
using namespace uqac::network;
using namespace uqac::game;

ReplicationManager::ReplicationManager(bool server, std::string ipAddress, int port) : 
	m_network(), m_isServer(server), m_ipAddress(ipAddress), m_port(port), m_serializer(), m_player(nullptr) {
	OnReceiveFunction onReceiveCallback = [this](TCPConnection*, std::vector<char> receivedMsg)->void { this->updateRemoteData(receivedMsg); };
	if (!server) {
		m_network.connect(ipAddress, port, true, false, onReceiveCallback);
	}
	else {
		OnConnectFunction onConnectCallback = [this](TCPConnection* connection)->void {
			std::cout << "Tracking new player" << std::endl;
			m_player = new Player();
			this->trackObject(m_player);
			if (connection->getNumberClients() >= 2) {
				std::cout << "Tracking new enemy" << std::endl;
				this->trackObject(new Enemy());
			};
			//We show the new clients all the objects
			showCurrentData(connection);
			//We show the other clients the new objects
			updateModifiedData(connection);
		};
		m_network.listen(port, ipAddress, true, false, onReceiveCallback, onConnectCallback);
	}
}

void ReplicationManager::trackObject(NetworkObject* object) {
	std::optional<int> id = m_linkingContext.getIDFromObject(object);
	if (!id.has_value()) {
		m_replicatedObjects.insert(object);
		m_linkingContext.addObject(object);
		m_modifiedObjects.push_back({ object, Hello });
	}
	else {
		std::cout << "Object already exists" << std::endl;
	}
}

void ReplicationManager::modifyRandomObject() {
	
	if (m_replicatedObjects.size() > 0) {
		std::random_device rd;     // only used once to initialise (seed) engine
		std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
		std::uniform_int_distribution<int> uni(0, m_replicatedObjects.size() - 1); // guaranteed unbiased

		int index = uni(rng);

		auto itr = m_replicatedObjects.begin();
		std::advance(itr, index);

		NetworkObject* toModify = *itr;

		toModify->modifyRandomAttribute();

		m_modifiedObjects.push_back({ toModify, Sync });
	}
	updateModifiedData();
}

void ReplicationManager::showDeletedObject(NetworkObject* object) {
	if (m_linkingContext.getIDFromObject(object).has_value()) {
		m_modifiedObjects.push_back({ object, Bye });
		updateModifiedData();
	}
	else {
		std::cerr << "Tried to remove an object that was never registered" << std::endl;
		exit(EXIT_FAILURE);
	}
}

//Protocol/MsgType/ClassID/ObjectID/Data

void ReplicationManager::showCurrentData(TCPConnection * connection) {
	//Write protocol
	m_serializer.Serialize<uint8_t>(TCP);
	//Write MsgType
	m_serializer.Serialize<uint8_t>(Hello);
	//Write ObjectID
	m_serializer.Serialize<int>(m_linkingContext.getIDFromObject(m_player).value());
	//Write ClassID
	m_serializer.Serialize<int>(m_player->getClassID());
	//Write Data
	m_player->write(m_serializer);
	for (auto object : m_replicatedObjects) {
		if (object != m_player) {
			//Write MsgType
			m_serializer.Serialize<uint8_t>(Hello);
			//Write ObjectID
			m_serializer.Serialize<int>(m_linkingContext.getIDFromObject(object).value());
			//Write ClassID
			m_serializer.Serialize<int>(object->getClassID());
			//Write Data
			object->write(m_serializer);
		}
	}
	if (m_isServer) {
		connection->sendMsgToClient(m_serializer.getContainer());
	}
	else {
		m_network.sendMsg(m_serializer.getContainer(), m_ipAddress, m_port);
	}
	m_serializer.clear();
}

void ReplicationManager::updateModifiedData(TCPConnection* exceptOn) {
	//Write protocol
	m_serializer.Serialize<uint8_t>(TCP);
	for (auto modifiedObject : m_modifiedObjects) {
		//Write MsgType
		m_serializer.Serialize<uint8_t>(modifiedObject.second);
		//Write ObjectID
		m_serializer.Serialize<int>(m_linkingContext.getIDFromObject(modifiedObject.first).value());
		//Write ClassID
		m_serializer.Serialize<int>(modifiedObject.first->getClassID());
		//Write Data
		modifiedObject.first->write(m_serializer);
	}
	if (m_isServer) {
		if (exceptOn != nullptr) {
			exceptOn->broadcastMsgToOthers(m_serializer.getContainer());
		}
		else {
			m_network.broadcastMsgOn(m_serializer.getContainer(), m_port);
		}
	}
	else {
		m_network.sendMsg(m_serializer.getContainer(), m_ipAddress, m_port);
	}
	m_serializer.clear();
	m_modifiedObjects.clear();
}

void ReplicationManager::updateRemoteData(std::vector<char> buffer) {

	if (!m_ignoreNextMsg) {

		std::cout << "Updating remote data" << std::endl;

		Deserializer deserializer = Deserializer(buffer);

		if (m_player == nullptr) {
			uint8_t msgType;
			if (deserializer.Deserialize<uint8_t>(msgType) == 0) {
				std::cerr << "No msg type found" << std::endl;
				exit(EXIT_FAILURE);
			}
			int objectID;
			if (deserializer.Deserialize<int>(objectID) == 0) {
				std::cerr << "No object ID found" << std::endl;
				exit(EXIT_FAILURE);
			}
			int classID;
			if (deserializer.Deserialize<int>(classID) == 0) {
				std::cerr << "No class ID found" << std::endl;
				exit(EXIT_FAILURE);
			}

			std::cout << "Discovering our assigned player :" << std::endl;

			m_player = ClassRegistry::create(classID);
			m_player->read(deserializer);
			m_linkingContext.addObjectAndID(m_player, objectID);
			m_replicatedObjects.insert(m_player);

			std::cout << m_player->toString() << std::endl;
		}

		while (!deserializer.isEmpty()) {
			uint8_t msgType;
			if (deserializer.Deserialize<uint8_t>(msgType) == 0) {
				std::cerr << "No msg type found" << std::endl;
				exit(EXIT_FAILURE);
			}
			int objectID;
			if (deserializer.Deserialize<int>(objectID) == 0) {
				std::cerr << "No object ID found" << std::endl;
				exit(EXIT_FAILURE);
			}
			int classID;
			if (deserializer.Deserialize<int>(classID) == 0) {
				std::cerr << "No class ID found" << std::endl;
				exit(EXIT_FAILURE);
			}
			if (msgType == Hello) {
				if (!m_linkingContext.getObjectFromID(objectID).has_value()) {
					std::cout << "Discovering new object :" << std::endl;

					NetworkObject* newObject = ClassRegistry::create(classID);
					newObject->read(deserializer);
					m_linkingContext.addObjectAndID(newObject, objectID);
					m_replicatedObjects.insert(newObject);

					std::cout << newObject->toString() << std::endl;
				}
			}
			else if (msgType == Sync) {
				std::cout << "Updating existing object :" << std::endl;

				std::optional<NetworkObject*> object = m_linkingContext.getObjectFromID(objectID);
				if (!object.has_value()) {
					std::cerr << "Tried to modify an object that was never registered in the linking context" << std::endl;
					exit(EXIT_FAILURE);
				}
				object.value()->read(deserializer);

				std::cout << object.value()->toString() << std::endl;
			}
			else if (msgType == Bye) {
				std::cout << "Deleting object :" << std::endl;

				//We only read the object to empty the deserializer
				NetworkObject* trash = ClassRegistry::create(classID);
				trash->read(deserializer);

				std::optional<NetworkObject*> object = m_linkingContext.getObjectFromID(objectID);
				if (!object.has_value()) {
					std::cerr << "Tried to delete an object that was never registered in the linking context" << std::endl;
					exit(EXIT_FAILURE);
				}

				std::cout << object.value()->toString() << std::endl;

				if (m_isServer) {
					showDeletedObject(object.value());
				}
				m_linkingContext.deleteObject(object.value());
				m_replicatedObjects.erase(object.value());
			}
		}
	}
}

ReplicationManager::~ReplicationManager() {
	if (!m_isServer) {
		//Say that our player must be destroyed
		//Write protocol
		m_serializer.Serialize<uint8_t>(TCP);
		//Write MsgType
		m_serializer.Serialize<uint8_t>(Bye);
		//Write ObjectID
		m_serializer.Serialize<int>(m_linkingContext.getIDFromObject(m_player).value());
		//Write ClassID
		m_serializer.Serialize<int>(m_player->getClassID());
		//Write Data
		m_player->write(m_serializer);

		m_network.sendMsg(m_serializer.getContainer(), m_ipAddress, m_port);

		m_serializer.clear();
	}
	m_replicatedObjects.clear();
	m_modifiedObjects.clear();
	delete(m_player);

	m_ignoreNextMsg = true;
}

void ReplicationManager::printObjects() {
	std::cout << std::endl << "Objects list : " << std::endl << std::endl;
	for (NetworkObject* object : m_replicatedObjects) {
		std::cout << object->toString() << std::endl << std::endl;
	}
}