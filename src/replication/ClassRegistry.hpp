#pragma once

#include <map>
#include <functional>
#include "NetworkObject.hpp"

namespace uqac::replication {
	class ClassRegistry {

	//Singleton requirements

    public:
        static ClassRegistry& getInstance()
        {
            static ClassRegistry instance;  // Guaranteed to be destroyed.
                                            // Instantiated on first use.
            return instance;
        }

    private:
        ClassRegistry() {}

    public:
        ClassRegistry(ClassRegistry const&) = delete;
        void operator=(ClassRegistry const&) = delete;

        // Note: Scott Meyers mentions in his Effective Modern
        //       C++ book, that deleted functions should generally
        //       be public as it results in better error messages
        //       due to the compilers behavior to check accessibility
        //       before deleted status

	//Important methods

	private:
		std::map<int, std::function<NetworkObject* ()>> m_mapIdConstructor;

	public:

		template<class T>
		static void addClass() {
            getInstance().m_mapIdConstructor[T::ClassID] = std::function<NetworkObject* ()>( [=] ()->NetworkObject* { return new T(); });
        }

		static NetworkObject* create(int classIdentifier);
	};
}
