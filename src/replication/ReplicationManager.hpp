#pragma once

#include <unordered_set>

#include "NetworkObject.hpp"
#include "LinkingContext.hpp"
#include "ClassRegistry.hpp"
#include "../network/Master.hpp"
#include "../serialize/IntCompressor.hpp"

namespace uqac::replication {
	using namespace uqac::network;
	using namespace uqac::serialize;
	class ReplicationManager {

	public:
		enum PacketType : uint8_t {
			Hello = 0x00,
			Sync = 0x01,
			Bye = 0x02
		};

		enum Protocol : uint8_t {
			TCP = 0x00,
			UDP = 0x01
		};

	private:

		std::unordered_set<NetworkObject*> m_replicatedObjects;

		LinkingContext m_linkingContext;
		Master m_network;

		Serializer m_serializer;

		std::vector<std::pair<NetworkObject*, PacketType>> m_modifiedObjects;

		//For the server, this will register the last added player
		//For the client, this will register which player we are
		NetworkObject* m_player;

		bool m_ignoreNextMsg = false;

	public:

		using OnReceiveFunction = std::function<void(TCPConnection*, std::vector<char>)>;
		using OnConnectFunction = std::function<void(TCPConnection*)>;

		const bool m_isServer;
		const std::string m_ipAddress;
		const int m_port;

		ReplicationManager(bool server, std::string distIpAddress, int distPort);

		~ReplicationManager();

		void trackObject(NetworkObject* object);
		void showDeletedObject(NetworkObject* object);
		void modifyRandomObject();

		void showCurrentData(TCPConnection* connection);
		void updateModifiedData(TCPConnection* exceptOn = nullptr);
		void updateRemoteData(std::vector<char> buffer);

		void printObjects();
	};
}
