#include "ClassRegistry.hpp"

using namespace uqac::replication;

NetworkObject* ClassRegistry::create(int classIdentifier) {
	if (getInstance().m_mapIdConstructor[classIdentifier]) {
		return getInstance().m_mapIdConstructor[classIdentifier]();
	}
	else {
		return nullptr;
	}
}