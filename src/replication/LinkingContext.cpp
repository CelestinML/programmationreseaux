#include "LinkingContext.hpp"

using namespace uqac::replication;

void LinkingContext::addObjectAndID(NetworkObject* networkObject, int ID) {
	m_map_id_object[ID] = networkObject;
	m_map_object_id[networkObject] = ID;
}

void LinkingContext::addObject(NetworkObject* networkObject) {
	bool ok = false;

	while (!ok) {
		if (m_map_id_object.count(m_nextID)) {
			m_nextID++;
		}
		else {
			m_map_id_object[m_nextID] = networkObject;
			m_map_object_id[networkObject] = m_nextID;
			m_nextID++;
			ok = true;
		}
	}
}
void LinkingContext::deleteObject(NetworkObject* networkObject) {
	m_map_object_id.erase(networkObject);
	for (auto const& [key,val] : m_map_id_object) {
		if (val == networkObject) {
			m_map_id_object.erase(key);
			break;
		}
	}
}
std::optional<int> LinkingContext::getIDFromObject(NetworkObject* networkObject) {
	if (m_map_object_id.count(networkObject)) {
		return std::optional<int>(m_map_object_id[networkObject]);
	}
	else {
		//{} represents an empty optional
		return {};
	}
}
std::optional<NetworkObject*> LinkingContext::getObjectFromID(int ID) {
	if (m_map_id_object.count(ID)) {
		return std::optional<NetworkObject*>(m_map_id_object[ID]);
	}
	else {
		//{} represents an empty optional
		return {};
	}
}