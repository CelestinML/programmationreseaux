#include <map>
#include <optional>
#include "NetworkObject.hpp"


namespace uqac::replication {
	class LinkingContext {
	private:
		std::map<int, NetworkObject*> m_map_id_object;
		std::map<NetworkObject*, int> m_map_object_id;
		int m_nextID = 0;
	public:

		void addObjectAndID(NetworkObject* networkObject, int ID);
		void addObject(NetworkObject * networkObject);
		void deleteObject(NetworkObject* networkObject);
		std::optional<int> getIDFromObject(NetworkObject* networkObject);
		std::optional<NetworkObject *> getObjectFromID(int ID);

	};
}