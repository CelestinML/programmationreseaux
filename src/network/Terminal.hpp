#pragma once

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <thread>
#include "TCPConnection.hpp"

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

namespace uqac::network {
	class Terminal {
	private:
		SOCKET m_listenSocket;
		int m_port;
		std::string m_address;
		std::vector<std::shared_ptr<TCPConnection>> m_clientConnections;

		std::function<void(TCPConnection*, std::vector<char>)> m_onReceiveCallback;
		std::function<void(TCPConnection*)> m_onConnectCallback;

		void initSocket();
		void bindSocket();
		void listenSocket();

		void updateClientList(std::shared_ptr<TCPConnection> newClient);

	public:

		const bool m_isIPv6;

		//! Will get the listening socket (server's local socket) via the server's getter
		Terminal(int localPort, std::string localIpAddress, bool IPv6);
		~Terminal();

		//! thread launched when the object is created to listen for any connection on listenSocket,
		//! and will give the created client sockets to the serverConnection
		std::shared_ptr<TCPConnection> acceptClient();

		SOCKET getListenSocket();

		std::shared_ptr<TCPConnection> update();

		void setCallback(std::function<void(TCPConnection*, std::vector<char>)> onReceiveCallback);
		void setOnConnectCallback(std::function<void(TCPConnection*)> onConnectCallback);

		inline int getNumberClients() { return m_clientConnections.size(); }

		void removeClientConnection(SOCKET toRemove);
	};

}