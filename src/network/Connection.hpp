#pragma once

//Network requirements
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <thread>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

//Additional libraries
#include <string>
#include <queue>

namespace uqac::network{
	class Connection {

	private:
		SOCKET m_localSocket;
		std::queue<std::vector<char>> m_recvMsg;
		std::string m_distIpAddress;
		int m_distPort;
		int m_localPort;

		void initSocket();
		void destroySocket();

	public:

		const bool m_isServer;
		const bool m_isIPv6;
		const bool m_isTCP;

		Connection(std::string distIpAddress = "127.0.0.1", int distPort = 42666, bool TCP = true, bool IPv6 = false);
		Connection(int localPort, bool TCP, bool IPv6);
		Connection(SOCKET serverSocket, bool IPv6, int localPort);
		~Connection();

		SOCKET getLocalSocket();
		std::string getDistIpAddress();
		int getDistPort();
		int getLocalPort();

		void setBlocking();
		void setNonBlocking();

		//! Only for client connections.
		virtual void sendMsg(std::vector<char> msg) = 0;

		//! Only for server connections.
		virtual void broadcastMsg(std::vector<char> msg) = 0;

		/*!	
			A thread that will be launched once TCPConnection or UDPConnection is created
			to receive messages into the m_recvMsg attribute.
			These messages will be accessible using the method readNextMessage.
		*/
		//virtual void receiveMessages() = 0;

		//! Read the message received first. Returns an empty string if there was no message to read.
		std::vector<char> readNextMessage();

		// Add a message to the queue of received messages to be read. 
		void addMessagetoQueue(std::vector<char> msg);

		// Returns : - 1 if a message was received
		//			 - 0 if the connection was closed
		//			 - -1 if an error occured
		virtual int update() = 0;
	};
}