#pragma once

#include <vector>
#include <memory>
#include "UDPConnection.hpp"
#include "TCPConnection.hpp"
#include "Terminal.hpp"
#include <thread>
#include <time.h>

namespace uqac::network {
	class Master {

	private:

		std::vector<std::shared_ptr<Connection>> m_connections;
		fd_set m_readfds;
		//fd_set m_writefds;
		//fd_set m_exceptfds;
		std::vector<std::shared_ptr<Terminal>> m_terminals;
		std::thread m_manageSockets;
		bool m_stop;
		timeval m_timeout; 

		int m_maxFd;

		void updateSets();

		void updateMaxFd();

		void update();

		void deleteConnection(std::shared_ptr<Connection> connection);

	public:

		Master(float timeout = 50);
		~Master();

		std::shared_ptr<Connection> connect(std::string distIpAddress, int distPort, bool TCP, bool IPv6, std::function<void(TCPConnection*, std::vector<char>)> onReceiveCallback = NULL);
		void listen(int localPort, std::string localIpAddress, bool TCP, bool IPv6, std::function<void(TCPConnection*, std::vector<char>)> onReceiveCallback = NULL, std::function<void(TCPConnection*)> onConnectCallback = NULL);

		//! WARNING : only for client connections that are already established (connectTo has been called)
		void sendMsg(std::vector<char> msg, std::string distIpAddress, int distPort);

		//! WARNING : only for server connections that are already established (startServerOn has been called)
		void broadcastMsgOn(std::vector<char> msg, int localPort);

		//! WARNING : only for client connections that are already established (connectTo has been called)
		//! Will return an empty string if there was no message to read.
		std::vector<char> readNextMessageOn(std::string distIpAddress, int distPort);

	};
}