#include "Connection.hpp"

using namespace uqac::network;

Connection::Connection(std::string distIpAddress, int distPort, bool TCP, bool IPv6) : m_isServer(false), m_isTCP(TCP), m_isIPv6(IPv6) {
	WSADATA wsaData;
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
	}

	initSocket();
	
	m_distIpAddress = distIpAddress;
	m_distPort = distPort;
}

Connection::Connection(int localPort, bool TCP, bool IPv6) : m_isServer(true), m_isTCP(TCP), m_isIPv6(IPv6) {
	initSocket();

	m_localPort = localPort;
}


Connection::Connection(SOCKET serverSocket, bool IPv6, int localPort) : m_isServer(true), m_isTCP(true), m_isIPv6(IPv6), m_distPort(0), m_localPort(localPort)
{
	m_localSocket = serverSocket;
}

Connection::~Connection() {
	destroySocket();
}

void Connection::initSocket() {
	
	int family;
	int socktype;
	int protocol;
	if (m_isIPv6)
		family = AF_INET6;
	else
		family = AF_INET;
	if (m_isTCP) {
		socktype = SOCK_STREAM;
		protocol = IPPROTO_TCP;
	}
	else {
		socktype = SOCK_DGRAM;
		protocol = IPPROTO_UDP;
	}
	m_localSocket = socket(family, socktype, protocol);
	//Check if socket was properly openned
	if (m_localSocket == SOCKET_ERROR) {
		std::cout << "Could not open the socket" << std::endl;
		WSACleanup();
		exit(EXIT_FAILURE);
	}

	setNonBlocking();

}

void Connection::destroySocket() {
	if (m_isTCP) {
		shutdown(m_localSocket, SD_BOTH);
	}
	closesocket(m_localSocket);
}

SOCKET Connection::getLocalSocket() { return m_localSocket; }

std::string Connection::getDistIpAddress() {
	if (m_isServer) {
		std::cout << "WARNING : Can't access dist IP address with a server connection" << std::endl;
		return "";
	}
	else {
		return m_distIpAddress;
	}
}

int Connection::getDistPort() {
	if (m_isServer) {
		std::cout << "WARNING : Can't access dist port with a server connection" << std::endl;
		return -1;
	}
	else {
		return m_distPort;
	}
}

int Connection::getLocalPort() {
	if (!m_isServer) {
		std::cout << "WARNING : Can't access local port with a client connection" << std::endl;
		return -1;
	}
	else {
		return m_localPort;
	}
}

void Connection::setBlocking() {
	u_long blocking = 0;

	ioctlsocket(m_localSocket, FIONBIO, &blocking);
}

void Connection::setNonBlocking() {
	u_long nonBlocking = 1;

	ioctlsocket(m_localSocket, FIONBIO, &nonBlocking);
}

std::vector<char> Connection::readNextMessage() {

	if (!m_recvMsg.empty()) {
		std::vector<char> nextMsg = m_recvMsg.front();
		m_recvMsg.pop();
		return nextMsg;
	}
	else {
		return {};
	}
}

void Connection::addMessagetoQueue(std::vector<char> msg) {
	m_recvMsg.push(msg);
}