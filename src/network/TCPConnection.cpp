#include "TCPConnection.hpp"

using namespace uqac::network;

#define DEFAULT_BUFLEN 1024

TCPConnection::TCPConnection(std::string distIpAddress, int distPort, bool IPv6) : Connection(distIpAddress, distPort, true, IPv6) //client
{
	m_connected = false;
	setBlocking();
	m_connectingThread = std::thread([this]() {
		connectToServer();
	});
	setCallback([this](TCPConnection* connection, std::vector<char> msg) -> void {
		addMessagetoQueue(msg);

		std::cout << "Client received : " << std::string(msg.begin(), msg.end()) << std::endl;

	});
}

TCPConnection::TCPConnection(SOCKET serverSocket, bool IPv6, int localPort, std::string localIpAddress, std::vector<std::shared_ptr<TCPConnection>> otherClients) : Connection(serverSocket, IPv6, localPort)//server
{
	m_otherClients = otherClients;
	setCallback([this](TCPConnection* connection, std::vector<char> msg) -> void {

		std::cout << "Server received : " << std::string(msg.begin(), msg.end()) << std::endl;

		this->broadcastMsgToOthers(msg);
	});
}

TCPConnection::~TCPConnection() {
	if (!m_isServer) {
		m_connectingThread.join();
	}
}

void TCPConnection::sendMsg(std::vector<char> msg)
{
	if (m_connected) {
		const char* sendbuf = reinterpret_cast<char*>(&msg[0]);

		int iResult = send(getLocalSocket(), sendbuf, sizeof(msg), 0);
		if (iResult == SOCKET_ERROR)
		{
			printf("send failed (sendmsg) with error: %d\n", WSAGetLastError());
			closesocket(getLocalSocket());
			WSACleanup();
			exit(EXIT_FAILURE);
		}
	}
	else {
		std::cout << "The client is not connected to a server, you can't send any message yet." << std::endl;
	}
}

void TCPConnection::broadcastMsg(std::vector<char> msg)
{
	sendMsgToClient(msg);
	broadcastMsgToOthers(msg);
}

void TCPConnection::broadcastMsgToOthers(std::vector<char> msg) {
	for (auto& otherClient : m_otherClients) {
		otherClient->sendMsgToClient(msg);
	}
}

void TCPConnection::sendMsgToClient(std::vector<char> msg) {
	char* sendBuf = new char[msg.size()];
	std::copy(msg.begin(), msg.end(), sendBuf);
	int iSendResult = send(getLocalSocket(), sendBuf, msg.size(), 0);
	if (iSendResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(getLocalSocket());
		WSACleanup();
		exit(EXIT_FAILURE);
	}
}


void TCPConnection::connectToServer()
{
	std::cout << "Trying to connect" << std::endl;

	sockaddr* serv;
	int servSize;

	int iResult;

	int distPort = getDistPort();
	std::string stringDistAddr = getDistIpAddress();
	PCSTR distAddr = stringDistAddr.c_str();
	if (m_isIPv6)
	{
		struct addrinfo* result = NULL,
			* ptr = NULL,
			hints;
		int iResult;

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET6;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		// Resolve the server address and port
		iResult = getaddrinfo(distAddr, std::to_string(getDistPort()).c_str(), &hints, &result);
		if (iResult != 0) {
			printf("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
			exit(EXIT_FAILURE);
		}

		serv = result->ai_addr;
		servSize = (int)result->ai_addrlen;
	}
	else
	{
		sockaddr_in serv_addr;
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(distPort);
		inet_pton(AF_INET, distAddr, &serv_addr.sin_addr);

		serv = (sockaddr*)&serv_addr;
		servSize = sizeof(*serv);
	}

	iResult = connect(getLocalSocket(), serv, servSize);
	if (iResult == SOCKET_ERROR) {
		printf("connect failed with error: %d\n", WSAGetLastError());
		closesocket(getLocalSocket());
		WSACleanup();
		exit(EXIT_FAILURE);
	}
	m_connected = true;
	setNonBlocking();
	std::cout << "Connected successfully" << std::endl;
}

void TCPConnection::addClient(std::shared_ptr<TCPConnection> otherClient) {
	m_otherClients.push_back(otherClient);
}

int TCPConnection::receiveMessage()
{
	SOCKET ConnectSocket = getLocalSocket();
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;
	
	iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);

	if (iResult == 0) {
		//Ignore
		return 1;
	}
	if (iResult > 0 && iResult < DEFAULT_BUFLEN)
	{
		uint8_t protocol = recvbuf[0];

		if (protocol != 0) {
			std::cerr << "received a non tcp msg" << std::endl;

			return 1;
		}
		
		std::vector<char> recvMsg = std::vector<char>(recvbuf + 1, recvbuf + iResult);

		m_onReceive(this, recvMsg);

		return 1;
	}
	else
	{
		if (WSAGetLastError() == WSAECONNRESET || WSAGetLastError() == WSAECONNABORTED) {
			disconnect();
			return 0;
		}
		printf("recv failed in server (TCPConnection) with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		
		return -1;
	}
}

void TCPConnection::disconnect() {
	if (m_isServer) {
		std::cout << "A client disconnected from server." << std::endl;
		for (auto& otherClient : m_otherClients) {
			otherClient->removeClientConnection(getLocalSocket());
		}
	}
	else {
		std::cout << "Server shutdown." << std::endl;
	}
	m_otherClients.clear();
}

void TCPConnection::removeClientConnection(SOCKET localSocket) {
	int i = 0;
	for (auto& client : m_otherClients) {
		if (client->getLocalSocket() == localSocket) {
			m_otherClients.erase(m_otherClients.begin() + i);
			i--;
		}
		i++;
	}
}

void TCPConnection::setCallback(std::function<void(TCPConnection*, std::vector<char>)> callback) {
	m_onReceive = callback;
}

int TCPConnection::update() {
	return receiveMessage();
}

bool TCPConnection::isConnected() {
	return m_connected;
}