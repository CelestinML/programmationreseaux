#include "Master.hpp"

using namespace uqac::network;

Master::Master(float timeout) {
	WSADATA wsaData;
	int iResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
	}

	m_maxFd = 0;

	m_stop = false;
	m_timeout.tv_usec = timeout*1000;
	m_timeout.tv_sec = 0;
	m_manageSockets = std::thread([this]() {update(); });
}

std::shared_ptr<Connection> Master::connect(std::string distIpAddress, int distPort, bool TCP, bool IPv6, std::function<void(TCPConnection*, std::vector<char>)> onReceiveCallback) {
	
	if (TCP) {
		std::shared_ptr<TCPConnection> newClient;
		newClient = std::make_shared<TCPConnection>(distIpAddress, distPort, IPv6);
		newClient->setCallback(onReceiveCallback);
		m_connections.push_back(newClient);
		updateMaxFd();
		return newClient;
	}
	else {
		std::shared_ptr<UDPConnection> newClient;
		newClient = std::make_shared<UDPConnection>(distIpAddress, distPort, IPv6);
		m_connections.push_back(newClient);
		updateMaxFd();
		return newClient;
	}
}

void Master::updateSets() {
	FD_ZERO(&m_readfds);
	//FD_ZERO(&m_writefds);
	//FD_ZERO(&m_exceptfds);
	for (auto& connection : m_connections) {
		FD_SET(connection->getLocalSocket(), &m_readfds);
	}
	for (auto& terminal : m_terminals) {
		FD_SET(terminal->getListenSocket(), &m_readfds);
	}
}

void Master::updateMaxFd() {
	int newMaxFd = 0;
	for (auto& connection : m_connections) {
		newMaxFd = max(newMaxFd, connection->getLocalSocket());
	}
	for (auto& terminal : m_terminals) {
		newMaxFd = max(newMaxFd, terminal->getListenSocket());
	}
	m_maxFd = newMaxFd;
}

void Master::listen(int localPort, std::string localIpAddress, bool TCP, bool IPv6, std::function<void(TCPConnection*, std::vector<char>)> onReceiveCallback, std::function<void(TCPConnection*)> onConnectCallback) {
	if (TCP) {
		std::shared_ptr<Terminal> newTerminal = std::make_shared<Terminal>(localPort, localIpAddress, IPv6);
		newTerminal->setCallback(onReceiveCallback);
		newTerminal->setOnConnectCallback(onConnectCallback);
		m_terminals.push_back(newTerminal);
	}
	else {
		std::shared_ptr<UDPConnection> newUDPServer = std::make_shared<UDPConnection>(localPort, IPv6);
		m_connections.push_back(newUDPServer);
	}
	updateMaxFd();
}

void Master::sendMsg(std::vector<char> msg, std::string distIpAddress, int distPort) {
	for (auto& connection : m_connections) {
		if (!connection->m_isServer) {
			if (connection->getDistIpAddress() == distIpAddress && connection->getDistPort() == distPort) {
				connection->sendMsg(msg);
				break;
			}
		}
	}
}

void Master::broadcastMsgOn(std::vector<char> msg, int localPort) {
	for (auto& connection : m_connections) {
		if (connection->m_isServer) {
			if (connection->getLocalPort() == localPort) {
				connection->broadcastMsg(msg);
				break;
			}
		}
	}
}

std::vector<char> Master::readNextMessageOn(std::string distIpAddress, int distPort) {
	for (auto& connection : m_connections) {
		if (!connection->m_isServer) {
			if (connection->getDistIpAddress() == distIpAddress && connection->getDistPort() == distPort) {
				return connection->readNextMessage();
				break;
			}
		}
	}
	return {};
}

void Master::update() {
	while (!m_stop) {
		updateSets();
		int ret = select(m_maxFd + 1, &m_readfds, nullptr, nullptr, &m_timeout);
		if (ret > 0) {
			for (auto& connection : m_connections) {
				if (connection != nullptr) {
					if (FD_ISSET(connection->getLocalSocket(), &m_readfds)) {
						int updateInfo = connection->update();
						if (connection->m_isTCP && updateInfo != 1) {
							if (connection->m_isServer) {
								std::cout << "A client disconnected. Closing its connection." << std::endl;
								deleteConnection(connection);
							}
							else {
								std::cout << "The TCP Server has closed the connection." << std::endl << "You can't send any message anymore." << std::endl;
								return;
							}
						}
						if (!connection->m_isServer && updateInfo != 1) {
							std::cout << "A UDP connection has been closed by the server" << std::endl;
							return;
						}
					}
				}
				else {
					deleteConnection(connection);
				}
			}
			for (auto& terminal : m_terminals) {
				if (FD_ISSET(terminal->getListenSocket(), &m_readfds)) {
					m_connections.push_back(terminal->update());
					updateMaxFd();
				}
			}
		}
	}
}

Master::~Master() {
	m_stop = true;
	m_manageSockets.join();
	m_connections.clear();
	m_terminals.clear();
	WSACleanup();
}

void Master::deleteConnection(std::shared_ptr<Connection> toDelete) {
	int i = 0;
	for (auto& connection : m_connections) {
		if (connection == nullptr || toDelete->getLocalSocket() == connection->getLocalSocket()) {
			m_connections.erase(m_connections.begin() + i);
			i--;
		}
		else if (TCPConnection *tcpConnection = dynamic_cast<TCPConnection*>(connection.get())) {
			tcpConnection->removeClientConnection(toDelete->getLocalSocket());
			for (auto& terminal : m_terminals) {
				terminal->removeClientConnection(toDelete->getLocalSocket());
			}
		}
		i++;
	}
}