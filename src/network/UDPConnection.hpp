#pragma once

#include "Connection.hpp"
#include <functional>

namespace uqac::network {

	/*!
		NOTE : The constructors will have to call the corresponding Connection constructor with TCP set to false.
			   This will also set the m_distIpAddress, and the m_distPort. These will be accessible using the corresponding getters.
	*/

	class UDPConnection : public Connection {

	private:
		std::thread m_receiveThread;
		bool m_stopReceiveMsg;
		std::vector<sockaddr> m_clients;
		std::function<void(UDPConnection*, std::vector<char>, sockaddr*)> m_onReceive;

		void addClient(sockaddr* clientSockAddr);
		void removeClient(sockaddr* clientSockAddr);
		std::string sockaddrToString(sockaddr* addr);

	public:

		UDPConnection(std::string distIpAddress, int distPort, bool IPv6);
		UDPConnection(int localPort, bool IPv6);
		void sendMsg(std::vector<char> msg);
		void broadcastMsg(std::vector<char> msg);
		void broadcastMsg(std::vector<char> msg, sockaddr *noSend);
		int receiveMessage();
		void addMsgToBuffer(UDPConnection* connection, std::vector<char> msg, sockaddr* distSockAddr);
		//void broadcastNextMsg(UDPConnection* connection, std::string msg, sockaddr* distSockAddr);

		void setCallback(std::function<void(UDPConnection*, std::vector<char>, sockaddr*)> callback);
		int update();
	};
}
