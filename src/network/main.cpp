#include "Master.hpp"

using namespace uqac::network;

std::string toUpper(std::string str) {
	std::string ret = str;
	std::transform(ret.begin(), ret.end(), ret.begin(), ::toupper);
	return ret;
}

int main(int argc, char* argv[]) {

	bool server;
	bool tcp;
	bool ipv6;
	int localPort;
	int distPort;
	std::string distIpAddress;

	bool inputOk;
	std::string input;

	if (argc == 1) {
		inputOk = false;
		while (!inputOk) {
			std::cout << "Do you want to start a server or client connection ?" << std::endl << "Enter S for server or C for client : ";
			std::cin >> input;
			inputOk = true;
			if (toUpper(input) == "S") {
				server = true;
			}
			else if (toUpper(input) == "C") {
				server = false;
			}
			else {
				std::cout << "Please respect the requested input format." << std::endl;
				inputOk = false;
			}
		}

		inputOk = false;
		while (!inputOk) {
			std::cout << "Do you want your connection to use IPv6 ? Else, we will use IPv4." << std::endl << "Enter Y if you want to use IPv6 or else N : ";
			std::cin >> input;
			inputOk = true;
			if (toUpper(input) == "Y") {
				ipv6 = true;
			}
			else if (toUpper(input) == "N") {
				ipv6 = false;
			}
			else {
				std::cout << "Please respect the requested input format." << std::endl;
				inputOk = false;
			}
		}

		inputOk = false;
		while (!inputOk) {
			std::cout << "Do you want to use TCP or UDP protocol ?" << std::endl << "Enter TCP or UDP : ";
			std::cin >> input;
			inputOk = true;
			if (toUpper(input) == "TCP") {
				tcp = true;
			}
			else if (toUpper(input) == "UDP") {
				tcp = false;
			}
			else {
				std::cout << "Please respect the requested input format." << std::endl;
				inputOk = false;
			}
		}

		if (server) {
			inputOk = false;
			while (!inputOk) {
				std::cout << "On which port do you want to connect ?" << std::endl << "Enter your requested port number : ";
				std::cin >> input;
				inputOk = true;
				try {
					localPort = std::stoi(input);
				}
				catch (std::invalid_argument) {
					std::cout << "Please enter an integer number without any additional character." << std::endl;
					inputOk = false;
				}
			}
		}
		else {
			inputOk = false;
			while (!inputOk) {
				std::cout << "On which IP address do you want to reach the server ?" << std::endl << "Enter this address : ";
				std::cin >> input;
				inputOk = true;
				int family;
				if (ipv6) {
					family = AF_INET6;
					sockaddr_in6 distSockAddr;
					if (inet_pton(family, input.c_str(), &distSockAddr.sin6_addr) == 1) {
						distIpAddress = input;
					}
					else {
						std::cout << "Please enter a valid IP address." << std::endl;
						inputOk = false;
					}
				}
				else {
					family = AF_INET;
					sockaddr_in distSockAddr;
					if (inet_pton(family, input.c_str(), &distSockAddr.sin_addr) == 1) {
						distIpAddress = input;
					}
					else {
						std::cout << "Please enter a valid IP address." << std::endl;
						inputOk = false;
					}
				}

			}

			inputOk = false;
			while (!inputOk) {
				std::cout << "On which port is the server connected ?" << std::endl << "Enter this port number : ";
				std::cin >> input;
				inputOk = true;
				try {
					distPort = std::stoi(input);
				}
				catch (std::invalid_argument) {
					std::cout << "Please enter an integer number without any additional character." << std::endl;
					inputOk = false;
				}
			}
		}
	}
	else if (argc == 4) {
		//Server
		std::string familyInput = argv[1];
		std::string protocolInput = argv[2];
		std::string localPortInput = argv[3];

		if (toUpper(familyInput) == "IPV6") {
			ipv6 = true;
		}
		else if (toUpper(familyInput) == "IPV4") {
			ipv6 = false;
		}
		else {
			std::cout << "Bad family format (ipv4 or ipv6)" << std::endl;
			exit(EXIT_FAILURE);
		}

		if (toUpper(protocolInput) == "TCP") {
			tcp = true;
		}
		else if (toUpper(protocolInput) == "UDP") {
			tcp = false;
		}
		else {
			std::cout << "Bad protocol format (udp or tcp)" << std::endl;
			exit(EXIT_FAILURE);
		}

		try {
			localPort = std::stoi(localPortInput);
		}
		catch (std::invalid_argument) {
			std::cout << "Please enter a whole number for the port" << std::endl;
			exit(EXIT_FAILURE);
		}

		server = true;
	}
	else if (argc == 5) {
		//Client

		std::string familyInput = argv[1];
		std::string protocolInput = argv[2];
		std::string distIpAddressInput = argv[3];
		std::string distPortInput = argv[4];

		if (toUpper(familyInput) == "IPV6") {
			ipv6 = true;
		}
		else if (toUpper(familyInput) == "IPV4") {
			ipv6 = false;
		}
		else {
			std::cout << "Bad family format (ipv4 or ipv6)" << std::endl;
			exit(EXIT_FAILURE);
		}

		if (toUpper(protocolInput) == "TCP") {
			tcp = true;
		}
		else if (toUpper(protocolInput) == "UDP") {
			tcp = false;
		}
		else {
			std::cout << "Bad protocol format (udp or tcp)" << std::endl;
			exit(EXIT_FAILURE);
		}

		int family;
		if (ipv6) {
			family = AF_INET6;
			sockaddr_in6 distSockAddr;
			if (inet_pton(family, distIpAddressInput.c_str(), &distSockAddr.sin6_addr) == 1) {
				distIpAddress = distIpAddressInput;
			}
			else {
				std::cout << "Please enter a valid IPv6 address." << std::endl;
				exit(EXIT_FAILURE);
			}
		}
		else {
			family = AF_INET;
			sockaddr_in distSockAddr;
			if (inet_pton(family, distIpAddressInput.c_str(), &distSockAddr.sin_addr) == 1) {
				distIpAddress = distIpAddressInput;
			}
			else {
				std::cout << "Please enter a valid IPv4 address." << std::endl;
				exit(EXIT_FAILURE);
			}
		}

		try {
			distPort = std::stoi(distPortInput);
		}
		catch (std::invalid_argument) {
			std::cout << "Please enter a whole number for the port" << std::endl;
			exit(EXIT_FAILURE);
		}

		server = false;
	}
	else {
		std::cout << "Please give 3 arguments for a server or 4 arguments for a client." << std::endl
			<< "- to start a server : ipv6/ipv4 udp/tcp localPort" << std::endl
			<< "- to start a client : ipv6/ipv4 udp/tcp distIpAddress distPort" << std::endl;

		exit(EXIT_FAILURE);
	}

	Master master = Master();
	std::shared_ptr<Connection> clientConnection;
	if (server) {
		master.listen(localPort, tcp, ipv6);
	}
	else {
		clientConnection = master.connect(distIpAddress, distPort, tcp, ipv6);
	}
	std::cout << "Connection started successfully." << std::endl;
	if (server) {
		std::cout << "Enter any message you would like to broadcast to your clients." << std::endl;
	}
	else {
		std::cout << "Enter any message you would like to send to the server." << std::endl;
	}
	std::cout << "If you want to stop the connection, enter EXIT." << std::endl;
	while (1) {
		std::getline(std::cin, input);
		if (toUpper(input) == "EXIT") {
			break;
		}
		else {
			if (server) {
				master.broadcastMsgOn(input, localPort);
			}
			else {
				clientConnection->sendMsg(input);
			}
		}
	}

}