#pragma once

#include "Connection.hpp"
#include <functional>

namespace uqac::network {

	/*!
		NOTE : The constructors will have to call the corresponding Connection constructor with TCP set to true.
			   This will also set the m_distIpAddress, and the m_distPort. These will be accessible using the corresponding getters.
	*/

	class TCPConnection : public Connection {

	private:

		std::vector<std::shared_ptr<TCPConnection>> m_otherClients;
		std::function<void(TCPConnection*, std::vector<char>)> m_onReceive;
		std::thread m_connectingThread;
		bool m_connected;

	public:

		TCPConnection(std::string distIpAddress, int distPort, bool IPv6);//client
		TCPConnection(SOCKET serverSocket, bool IPv6, int localPort, std::string localIpAddress, std::vector<std::shared_ptr<TCPConnection>> otherClients);//server
		~TCPConnection();

		//Only for clients : will check if the connect has been completed or not
		bool isConnected();

		void addClient(std::shared_ptr<TCPConnection> otherClient);

		void sendMsg(std::vector<char> msg);
		void broadcastMsg(std::vector<char> msg);
		void broadcastMsgToOthers(std::vector<char> msg);
		
		int receiveMessage();
		void connectToServer();
		void sendMsgToClient(std::vector<char> msg);

		void TCPConnection::disconnect();
		void TCPConnection::removeClientConnection(SOCKET localSocket);

		void setCallback(std::function<void(TCPConnection*, std::vector<char>)> callback);
		int update();

		inline int getNumberClients() { return m_otherClients.size() + 1; }
	};
}