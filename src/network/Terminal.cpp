#include "Terminal.hpp"

using namespace uqac::network;

#define DEFAULT_BUFLEN 1024

Terminal::Terminal(int localPort, std::string localIpAddress, bool IPv6) : m_isIPv6(IPv6)
{
	m_port = localPort;
	m_address = localIpAddress;

	initSocket();
	bindSocket();

	listenSocket();
}

void Terminal::updateClientList(std::shared_ptr<TCPConnection> newClient) {
	for (auto& otherClient : m_clientConnections) {
		otherClient->addClient(newClient);
	}
}

std::shared_ptr<TCPConnection> Terminal::acceptClient()
{
	SOCKET ClientSocket = INVALID_SOCKET;

	ClientSocket = accept(getListenSocket(), NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(getListenSocket());
		WSACleanup();
		exit(EXIT_FAILURE);
	}
	else
	{
		std::cout << "Client properly accepted" << std::endl;
		std::shared_ptr<TCPConnection> newClient = std::make_unique<TCPConnection>(ClientSocket, m_isIPv6, m_port, m_address, m_clientConnections);
		newClient->setCallback(m_onReceiveCallback);
		updateClientList(newClient);
		m_clientConnections.push_back(newClient);
		m_onConnectCallback(newClient.get());
		return newClient;
	}
}

void Terminal::initSocket() {
	
	int family;
	if (m_isIPv6)
		family = AF_INET6;
	else
		family = AF_INET;
	
	m_listenSocket = socket(family, SOCK_STREAM, IPPROTO_TCP);

	//Check if socket was properly openned
	if (m_listenSocket == SOCKET_ERROR) {
		std::cout << "Could not open the socket" << std::endl;
		WSACleanup();
		exit(EXIT_FAILURE);
	}

	u_long nonBlocking = 1;
	ioctlsocket(m_listenSocket, FIONBIO, &nonBlocking);
}

void Terminal::bindSocket()
{
	if (m_isIPv6) {
		struct addrinfo* result = NULL,
			* ptr = NULL,
			hints;
		int iResult;

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET6;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		// Resolve the server address and port
		iResult = getaddrinfo("::1", std::to_string(m_port).c_str(), &hints, &result);
		if (iResult != 0) {
			printf("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
			exit(EXIT_FAILURE);
		}

		// Bind the socket
		iResult = bind(m_listenSocket, result->ai_addr, (int)result->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			printf("bind failed with error: %d\n", WSAGetLastError());
			closesocket(m_listenSocket);
			WSACleanup();
			exit(EXIT_FAILURE);
		}
	}
	else {
		sockaddr_in serv_addr;
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(m_port);
		inet_pton(AF_INET, m_address.c_str(), &serv_addr.sin_addr);

		// Bind the socket
		int iResult = bind(getListenSocket(), (sockaddr*)&serv_addr, sizeof(sockaddr));
		if (iResult == SOCKET_ERROR) {
			printf("bind failed with error: %d\n", WSAGetLastError());
			closesocket(getListenSocket());
			WSACleanup();
			exit(EXIT_FAILURE);
		}
	} 
}

void Terminal::listenSocket()
{
	int iResult;
	iResult = listen(getListenSocket(), SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(getListenSocket());
		WSACleanup();
	}
}

Terminal::~Terminal() 
{
	shutdown(m_listenSocket, SD_BOTH);
	closesocket(m_listenSocket);
}

SOCKET Terminal::getListenSocket() 
{
	return m_listenSocket;
}

std::shared_ptr<TCPConnection> Terminal::update() {
	return acceptClient();
}

void Terminal::setCallback(std::function<void(TCPConnection*, std::vector<char>)> onReceiveCallback) {
	m_onReceiveCallback = onReceiveCallback;
}

void Terminal::setOnConnectCallback(std::function<void(TCPConnection*)> onConnectCallback) {
	m_onConnectCallback = onConnectCallback;
}

void Terminal::removeClientConnection(SOCKET toRemove) {
	int i = 0;
	for (auto& clientConnection : m_clientConnections) {
		if (clientConnection->getLocalSocket() == toRemove) {
			m_clientConnections.erase(m_clientConnections.begin() + i);
			i--;
		}
		i++;
	}
}