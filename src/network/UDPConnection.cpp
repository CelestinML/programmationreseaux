#include <thread>
#include "UDPConnection.hpp"

using namespace uqac::network;

#define DEFAULT_BUFLEN 1024

UDPConnection::UDPConnection(std::string distIpAddress, int distPort, bool IPv6) : Connection(distIpAddress, distPort, false, IPv6) {
	setCallback([this](UDPConnection* connection, std::vector<char> msg, sockaddr* distSockAddr) -> void {
		//addMsgToBuffer(connection, msg, distSockAddr);
		std::cout << "Client received : " << std::string(msg.begin(), msg.end()) << std::endl;
	});
}

UDPConnection::UDPConnection(int localPort, bool IPv6) : Connection(localPort, false, IPv6) {

	SOCKET ServerSocket = getLocalSocket();

	if (IPv6) {
		struct addrinfo* result = NULL,
			* ptr = NULL,
			hints;
		int iResult;

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET6;
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_protocol = IPPROTO_UDP;

		// Resolve the server address and port
		iResult = getaddrinfo("::1", std::to_string(localPort).c_str(), &hints, &result);
		if (iResult != 0) {
			printf("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
			exit(EXIT_FAILURE);
		}

		// Bind the socket
		iResult = bind(ServerSocket, result->ai_addr, (int)result->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			printf("bind failed with error: %d\n", WSAGetLastError());
			closesocket(ServerSocket);
			WSACleanup();
			exit(EXIT_FAILURE);
		}
	}
	else {
		sockaddr_in serv_addr;
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(localPort);
		inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);

		// Bind the socket
		int iResult = bind(ServerSocket, (sockaddr*)&serv_addr, sizeof(sockaddr));
		if (iResult == SOCKET_ERROR) {
			printf("bind failed with error: %d\n", WSAGetLastError());
			closesocket(ServerSocket);
			WSACleanup();
			exit(EXIT_FAILURE);
		}
	}

	setCallback([this](UDPConnection* connection, std::vector<char> msg, sockaddr* distSockAddr) -> void {
		std::cout << "Server received : " << std::string(msg.begin(), msg.end()) << std::endl;
		this->broadcastMsg(msg, distSockAddr);
	});
}


std::string UDPConnection::sockaddrToString(sockaddr* addr) {
	std::string ret;
	if (addr->sa_family == AF_INET) {
		sockaddr_in* ipv4_addr = (sockaddr_in*)addr;
		char ip[INET_ADDRSTRLEN];
		inet_ntop(AF_INET, &(ipv4_addr->sin_addr), ip, INET_ADDRSTRLEN);
		std::string port = std::to_string(ipv4_addr->sin_port);
		ret = ip;
		ret += ":";
		ret += port;
	}
	else {
		sockaddr_in6* ipv6_addr = (sockaddr_in6*)addr;
		char ip[INET6_ADDRSTRLEN];
		inet_ntop(AF_INET6, &(ipv6_addr->sin6_addr), ip, INET6_ADDRSTRLEN);
		std::string port = std::to_string(ipv6_addr->sin6_port);
		ret = ip;
		ret += ":";
		ret += port;
	}
	return ret;
}

void UDPConnection::addClient(sockaddr* clientSockAddr) {
	
	std::string newClientIp = sockaddrToString(clientSockAddr);
	for (sockaddr registeredClient : m_clients) {
		if (newClientIp == sockaddrToString(&registeredClient)) {
			//We have already registered the client, we exit the function
			return;
		}
	}
	std::cout << "Client " << sockaddrToString(clientSockAddr) << " added to the list." << std::endl;
	//If we got here, it means that the client was never registered
	m_clients.push_back(*clientSockAddr);
}

void UDPConnection::removeClient(sockaddr* clientSockAddr) {
	std::cout << "removing a client" << std::endl;
	std::string toDeleteIp = sockaddrToString(clientSockAddr);
	int i = -1;
	for (sockaddr registeredClient : m_clients) {
		i++;
		if (toDeleteIp == sockaddrToString(&registeredClient)) {
			m_clients.erase(m_clients.begin() + i);
		}
	}
}

void UDPConnection::sendMsg(std::vector<char> msg) {
	if (!m_isServer) {
		//std::cout << "Client " << getLocalSocket() << " sends the msg " << msg << std::endl;
		char* sendBuf = new char[msg.size()];
		std::copy(msg.begin(), msg.end(), sendBuf);
		SOCKET ConnectSocket = getLocalSocket();
		std::string s = getDistIpAddress();
		PCSTR distAddr = s.c_str();
		sockaddr *sendSockAddr;
		int sendSockAddrLen;
		if (m_isIPv6) {
			struct addrinfo* result = NULL,
				* ptr = NULL,
				hints;
			int iResult;

			ZeroMemory(&hints, sizeof(hints));
			hints.ai_family = AF_INET6;
			hints.ai_socktype = SOCK_DGRAM;
			hints.ai_protocol = IPPROTO_UDP;

			// Resolve the server address and port
			iResult = getaddrinfo(distAddr, std::to_string(getDistPort()).c_str(), &hints, &result);
			if (iResult != 0) {
				printf("getaddrinfo failed with error: %d\n", iResult);
				WSACleanup();
				exit(EXIT_FAILURE);
			}

			sendSockAddr = result->ai_addr;
			sendSockAddrLen = (int)result->ai_addrlen;
		}
		else {
			sockaddr_in distSockAddr;
			distSockAddr.sin_family = AF_INET;
			distSockAddr.sin_port = htons(getDistPort());
			inet_pton(AF_INET, distAddr, &distSockAddr.sin_addr);

			sendSockAddr = (sockaddr*)&distSockAddr;
			sendSockAddrLen = sizeof(*sendSockAddr);
		}

		int iResult = sendto(ConnectSocket, sendBuf, msg.size(), 0, sendSockAddr, sendSockAddrLen);
		if (iResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			exit(EXIT_FAILURE);
		}
		
	}
}

void UDPConnection::broadcastMsg(std::vector<char> msg) {
	int sockAddrLen;
	if (m_isIPv6) {
		sockAddrLen = sizeof(SOCKADDR_STORAGE);
	}
	else {
		sockAddrLen = sizeof(sockaddr_in);
	}
	for (sockaddr clientSockAddr : m_clients) {
		char* sendBuf = new char[msg.size()];
		std::copy(msg.begin(), msg.end(), sendBuf);
		int iSendResult = sendto(getLocalSocket(), sendBuf, msg.size(), 0, &clientSockAddr, sockAddrLen);
		if (iSendResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(getLocalSocket());
			WSACleanup();
			exit(EXIT_FAILURE);
		}
	}
}

void UDPConnection::broadcastMsg(std::vector<char> msg, sockaddr *noSend) {
	int sockAddrLen;
	if (m_isIPv6) {
		sockAddrLen = sizeof(sockaddr_in6);
	}
	else {
		sockAddrLen = sizeof(sockaddr_in);
	}
	for (sockaddr clientSockAddr : m_clients) {
		if (sockaddrToString(noSend) != sockaddrToString(&clientSockAddr)) {
			char* sendBuf = new char[msg.size()];
			std::copy(msg.begin(), msg.end(), sendBuf);
			int iSendResult = sendto(getLocalSocket(), sendBuf, msg.size(), 0, &clientSockAddr, sockAddrLen);
			if (iSendResult == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
				closesocket(getLocalSocket());
				WSACleanup();
				exit(EXIT_FAILURE);
			}
		}
	}
}

int UDPConnection::receiveMessage() {
	SOCKET ConnectSocket = getLocalSocket();
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;
	
	if (m_isServer) {
		//SERVER

		sockaddr* clientSockAddr;
		socklen_t clientAddrLength;

		if (m_isIPv6) {
			SOCKADDR_STORAGE clientSockAddr6;
			clientSockAddr = (sockaddr*)&clientSockAddr6;
			clientAddrLength = sizeof(clientSockAddr6);
		}
		else {
			sockaddr_in clientSockAddr4;
			clientSockAddr = (sockaddr*)&clientSockAddr4;
			clientAddrLength = sizeof(clientSockAddr4);
		}
		
		//std::cout << "Waiting for msg in server " << std::endl;
		iResult = recvfrom(ConnectSocket, recvbuf, recvbuflen, 0, clientSockAddr, &clientAddrLength);
		if (iResult == 0) {
			//Ignore
			return 1;
		}
		else if (iResult > 0 && iResult < DEFAULT_BUFLEN) {
			
			//Save the client's infos
			addClient(clientSockAddr);

			recvbuf[iResult] = '\0';
				
			m_onReceive(this, std::vector<char>(recvbuf, recvbuf + recvbuflen), clientSockAddr);

			return 1;
		}
		else {
			if (WSAGetLastError() == WSAECONNRESET) {
				std::cout << "Client " << sockaddrToString(clientSockAddr) << " has disconnected. Removing him from the client list." << std::endl;
				removeClient(clientSockAddr);
				return 0;
			}
			printf("recv failed in server with error: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			return -1;
		}
	}
	else {
		//CLIENT

		std::string s = getDistIpAddress();
		PCSTR distAddr = s.c_str();
		sockaddr* distSockAddr;
		int serverSockAddrLen;

		if (m_isIPv6) {
			struct addrinfo* result = NULL,
				* ptr = NULL,
				hints;
			int iResult;

			ZeroMemory(&hints, sizeof(hints));
			hints.ai_family = AF_INET6;
			hints.ai_socktype = SOCK_DGRAM;
			hints.ai_protocol = IPPROTO_UDP;

			// Resolve the server address and port
			iResult = getaddrinfo(distAddr, std::to_string(getDistPort()).c_str(), &hints, &result);
			if (iResult != 0) {
				printf("getaddrinfo failed with error: %d\n", iResult);
				WSACleanup();
				exit(EXIT_FAILURE);
			}

			distSockAddr = result->ai_addr;
			serverSockAddrLen = (int)result->ai_addrlen;
		}
		else {
			sockaddr_in distSockAddr4;
			distSockAddr4.sin_family = AF_INET;
			distSockAddr4.sin_port = htons(getDistPort());
			inet_pton(AF_INET, distAddr, &distSockAddr4.sin_addr);

			distSockAddr = (sockaddr*)&distSockAddr4;
			serverSockAddrLen = sizeof(*distSockAddr);
		}

		iResult = recvfrom(ConnectSocket, recvbuf, recvbuflen, 0, distSockAddr, &serverSockAddrLen);
		if (iResult == 0) {
			//Ignore
			return 1;
		}
		else if (iResult > 0 && iResult < DEFAULT_BUFLEN) {
			recvbuf[iResult] = '\0';
			
			m_onReceive(this, std::vector<char>(recvbuf, recvbuf + recvbuflen), distSockAddr);

			return 1;
		}
		else {
			if (WSAGetLastError() == WSAECONNRESET) {
				return 0;
			}
			std::cout << "recv failed in client " << getLocalSocket() << " with error: " << WSAGetLastError() << std::endl;
			closesocket(ConnectSocket);
			WSACleanup();
			return -1;
		}
	}
}

void UDPConnection::addMsgToBuffer(UDPConnection *connection, std::vector<char> msg, sockaddr *distSockAddr) {
	connection->addMessagetoQueue(msg);
}

//void UDPConnection::broadcastNextMsg(UDPConnection* connection, std::string msg, sockaddr* distSockAddr) {
//	connection->broadcastMsg(msg, distSockAddr);
//}

void UDPConnection::setCallback(std::function<void(UDPConnection*, std::vector<char>, sockaddr*)> callback) {
	m_onReceive = callback;
}

int UDPConnection::update() {
	return receiveMessage();
}