#include <iostream>
#include <iomanip>
#include <vector>

#include "replication/ClassRegistry.hpp"
#include "serialize/EnemyCompressor.h"
#include "serialize/PlayerCompressor.hpp"
#include "replication/ReplicationManager.hpp"

using namespace std;
using namespace uqac::serialize;
using namespace uqac::game;
using namespace uqac::replication;



std::string toUpper(std::string str) {
	std::string ret = str;
	std::transform(ret.begin(), ret.end(), ret.begin(), ::toupper);
	return ret;
}

int main(int argc, char* argv[]) {

	//Setup for the ClassRegistry singleton
	ClassRegistry::addClass<Player>();
	ClassRegistry::addClass<Enemy>();

	bool server;
	int port;
	std::string ipAddress;

	bool inputOk;
	std::string input;

	if (argc == 1) {
		inputOk = false;
		while (!inputOk) {
			std::cout << "Do you want to start a server or client connection ?" << std::endl << "Enter S for server or C for client : ";
			std::cin >> input;
			inputOk = true;
			if (toUpper(input) == "S") {
				server = true;
			}
			else if (toUpper(input) == "C") {
				server = false;
			}
			else {
				std::cout << "Please respect the requested input format." << std::endl;
				inputOk = false;
			}
		}

		inputOk = false;
		while (!inputOk) {
			std::cout << "On which port do you want to connect ?" << std::endl << "Enter your requested port number : ";
			std::cin >> input;
			inputOk = true;
			try {
				port = std::stoi(input);
			}
			catch (std::invalid_argument) {
				std::cout << "Please enter an integer number without any additional character." << std::endl;
				inputOk = false;
			}
		}

		inputOk = false;
		while (!inputOk) {
			std::cout << "On which IP address do you want to connect ?" << std::endl << "Enter this address : ";
			std::cin >> input;
			inputOk = true;
			int family;
			family = AF_INET;
			sockaddr_in distSockAddr;
			if (inet_pton(family, input.c_str(), &distSockAddr.sin_addr) == 1) {
				ipAddress = input;
			}
			else {
				std::cout << "Please enter a valid IP address." << std::endl;
				inputOk = false;
			}
		}
	}
	else if (argc == 4) {
		std::string serverOrClient = argv[1];

		if (toUpper(serverOrClient) == "S") {
			server = true;
		}
		else {
			server = false;
		}
		std::string tempAddress = argv[2];
		std::string tempPort = argv[3];

		try {
			port = std::stoi(tempPort);
		}
		catch (std::invalid_argument) {
			std::cout << "Please enter a whole number for the port" << std::endl;
			exit(EXIT_FAILURE);
		}

		int family = AF_INET;
		sockaddr_in distSockAddr;
		if (inet_pton(family, tempAddress.c_str(), &distSockAddr.sin_addr) == 1) {
			ipAddress = tempAddress;
		}
		else {
			std::cout << "Please enter a valid IPv4 address." << std::endl;
			exit(EXIT_FAILURE);
		}
	}
	else {
		std::cout << "Please enter either :" << std::endl
			<< "- 0 arguments to let us guide your setup choices" << std::endl
			<< "- 3 arguments in the following format : <S for server or C for client> <ipAddress> <portNumber>" << std::endl;

		exit(EXIT_FAILURE);
	}

	if (server) {
		ReplicationManager replicationManager = ReplicationManager(true, ipAddress, port);
		std::cout << "Connection started successfully." << std::endl;

		std::cout << "If you want to stop the connection, enter EXIT." << std::endl
			<< "If you want to modify a random object, enter UPDATE." << std::endl
			<< "If you want to print the current object, enter PRINT." << std::endl;

		while (1) {
			std::getline(std::cin, input);
			if (toUpper(input) == "UPDATE") {
				replicationManager.modifyRandomObject();
			}
			else if (toUpper(input) == "EXIT") {
				break;
			}
			else if (toUpper(input) == "PRINT") {
				replicationManager.printObjects();
			}
		}
	}
	else {
		ReplicationManager replicationManager = ReplicationManager(false, ipAddress, port);
		std::cout << "Connection started successfully." << std::endl;

		std::cout << "If you want to stop the connection, enter EXIT." << std::endl
			<< "If you want to print the current object, enter PRINT." << std::endl;
		while (1) {
			std::getline(std::cin, input);
			if (toUpper(input) == "EXIT") {
				break;
			}
			else if (toUpper(input) == "PRINT") {
				replicationManager.printObjects();
			}
		}
	}
}