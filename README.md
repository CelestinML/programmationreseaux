# ProgrammationRéseaux

## Comment configurer notre projet ?

Télécharger la dernière version du projet sur git

Ouvrir Visual Studio 2019

Sélectionner "Continuer sans code"

Sélectionner le menu Fichier>Ouvrir>CMAKE et sélectionner le fichier CMakeLists.txt à la racine de notre projet et cliquer sur "Ouvrir"

Dans le menu Projet, cliquer sur "Configurer ProgrammationReseaux"

Dans le menu Generer, selectionner "Tout Generer"

Le projet est maintenant compilé, et il ne reste plus qu'à l'exécuter.

## Comment tester notre projet ?

Lorsque le projet est ouvert et généré, se rendre à la page main.cpp et peser sur ctrl+f5 pour lancer le main.
Bien s'assurer que le document actif est soit le main.cpp ou le ReplicationTest.exe

## Analyser la validité de notre projet

Pour notre projet, il y a deux manières de tester: par la commande de ligne ou directement avec l'executable.

Pour la commande de ligne, il suffit de se rendre dans out\build\x64-Debug\src à partir de la racine du projet
et écrire:
ReplicationTest.exe &1 &2 &3 où:
&1 est S (pour serveur) ou C (pour client)
&2 est l'addresse IP
&3 est le port

Pour l'executable, il faut se rendre dans out\build\x64-Debug\src à partir de la racine du projet et double-cliquer
sur ReplicationTest.exe. Ensuite, il faut suivre les indications sur la console qui s'affichera pour l'information
client/serveur. 

Lorsque nous sommes serveur, nous avons accès à ses commandes:
UPDATE -> modifie une valeur aléatoire d'un objet aléatoire (Player, Enemy).
PRINT -> pour afficher tous les objets et leurs valeurs actuelles.
EXIT -> ferme le serveur et déconnecte tout les clients.

Lorsque nous sommes client, nous avons accès à ses commandes:
PRINT -> pour afficher tous les objets et leurs valeurs actuelles.
EXIT -> ferme le client et le déconnecte du serveur (ce qui supprime le Player de ce client aux autres clients aussi).
 
## Note

Nous n'avons pas eu le temps de réorganiser notre projet sur git. Par contre, nous avons essayer de simplifier
la tâche en ajoutant un tag intitulé "RenduTP3", qui sera le rendu final de notre troisième projet.